# Test project for GitLab code owner apporvals

This project is to test GitLab code owner approvals, specifically the option "Remove approvals by Code Owners if their files changed" in a merge request after a force push.
